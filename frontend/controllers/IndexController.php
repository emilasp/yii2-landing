<?php
namespace emilasp\landing\frontend\controllers;

use emilasp\core\helpers\SiteHelper;

use emilasp\landing\common\components\LandingDynamicAsset;
use emilasp\landing\common\models\LandDomain;
use emilasp\landing\common\models\Landing;
use emilasp\landing\common\models\LandPage;
use yii;
use emilasp\core\components\base\Controller;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;

/**
 * IndexController
 */
class IndexController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class'        => AccessControl::className(),
                'only'         => ['index'],
                'rules'        => [
                    [
                        'actions' => [
                            'index',
                        ],
                        'allow'   => true,
                        'roles'   => ['@', '?'],
                    ],
                ],
                'denyCallback' => Yii::$app->getModule('user')->denyCallback,
            ],
            'verbs'  => [
                'class'   => VerbFilter::className(),
                'actions' => [
                    //'delete' => ['POST'],
                ],
            ],
        ];
    }


    /**
     * Lists all Goal models.
     * @return mixed
     */
    public function actionIndex($page)
    {
        $this->layout = 'layout';

        /** Получаем domain, subdomain, page */
        /** Ищем по домену и поддомену лендиг */
        /** Ищем по page страницу лендинга*/
        /** Формируем страницу лендинга*/

        $dataUrl = SiteHelper::parseUrl($_SERVER['HTTP_HOST']);

        $domain = LandDomain::findOne(['domain' => $dataUrl['domain'], 'sub' => $dataUrl['sub']]);

        if (!$domain) {
            throw new yii\web\ForbiddenHttpException('Allow Deny');
        }

        $landings = Landing::findAll(['domain_id' => $domain->id]);

        if (!count($landings)) {
            throw new yii\web\ForbiddenHttpException('Allow Deny');
        }

        $landing = $landings[0];
        /* @TODO Добавить A/B тестирование */

        $page = LandPage::findOne(['landing_id' => $landing->id, 'page' => $page]);

        if (!$page) {
            throw new yii\web\ForbiddenHttpException('Allow Deny');
        }

        LandingDynamicAsset::$cssFiles = json_decode($page->css, true);
        LandingDynamicAsset::$jsFiles  = json_decode($page->script, true);
        LandingDynamicAsset::$source   = $landing->uploadPath;
        LandingDynamicAsset::register($this->view);

        $this->view->title             = $page->title;
        $this->view->params['landing'] = $landing;
        $this->view->params['page']    = $page;

        return $this->render('index', [
            'content' => $this->renderFile($page->view, [
                'landing' => $landing,
                'page'    => $page,
            ]),
        ]);
    }
}

<?php
namespace emilasp\landing\frontend;

use emilasp\core\CoreModule;

/**
 * Class LandingModule
 * @package emilasp\landing\frontend
 */
class LandingModule extends CoreModule
{
    public function init()
    {
        parent::init();

    }
}

<?php
namespace emilasp\landing\frontend\widgets\PageMetaWidget;

/** Виджет выводит мета и инлайн js в head
 * Class PageMetaWidget
 * @package emilasp\landing\frontend\widgets\PageMetaWidget
 */
class PageMetaWidget extends \yii\base\Widget
{
    public $landing;
    public $page;

    public function init()
    {
        parent::init();

    }

    public function run()
    {
        if ($this->landing->counters) {
            echo $this->landing->counters . PHP_EOL;
        }

        $meta = json_decode($this->page->meta, true);

        if ($meta) {
            foreach ($meta as $metaTag) {
                echo $metaTag['meta'] . PHP_EOL;
            }
        }


        if ($this->page->js) {
            echo $this->page->js . PHP_EOL;
        }
    }
}

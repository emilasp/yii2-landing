<?php

/* @var $this \yii\web\View */
/* @var $content string */

use emilasp\site\common\extensions\FlashMsg\FlashMsg;
use yii\helpers\Html;
use emilasp\landing\frontend\widgets\PageMetaWidget\PageMetaWidget;
?>

<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <?= Html::csrfMetaTags() ?>

    <title><?= Html::encode($this->title) ?></title>

    <?= PageMetaWidget::widget(['landing' => $this->params['landing'], 'page' => $this->params['page']]) ?>

    <?php $this->head() ?>

</head>
<body>
<?php $this->beginBody() ?>

<?php FlashMsg::widget(); ?>

<?= $content ?>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>


<?php

use emilasp\variety\models\Variety;
use yii\db\Migration;
use emilasp\core\helpers\FileHelper;

class m160712_224416_AddTAblesForlLanding extends Migration
{
    private $tableOptions = null;
    private $time;
    private $memory;

    /**
     * Project
     *
     * Domain
     *
     * Landing
     */
    public function up()
    {
        $this->createTable('landings_project', [
            'id'          => $this->primaryKey(11),
            'name'        => $this->string(255)->notNull(),
            'description' => $this->text(),
            'type'        => $this->smallInteger(1)->notNull(), //simple
            'status'      => $this->smallInteger(1)->notNull(),
            'created_at'  => $this->dateTime(),
            'updated_at'  => $this->dateTime(),
            'created_by'  => $this->integer(11),
            'updated_by'  => $this->integer(11),
        ], $this->tableOptions);

        $this->addForeignKey(
            'fk_landings_project_created_by',
            'landings_project',
            'created_by',
            'users_user',
            'id'
        );
        $this->addForeignKey(
            'fk_landings_project_updated_by',
            'landings_project',
            'updated_by',
            'users_user',
            'id'
        );

        $this->createTable('landings_domain', [
            'id'          => $this->primaryKey(11),
            'project_id'  => $this->integer(11),
            'name'        => $this->string(255)->notNull(),
            'domain'      => $this->string(30)->notNull(),
            'sub'         => $this->string(15)->notNull(),
            'description' => $this->text(),
            'type'        => $this->smallInteger(1)->notNull(), // simple
            'status'      => $this->smallInteger(1)->notNull(),
            'created_at'  => $this->dateTime(),
            'updated_at'  => $this->dateTime(),
            'created_by'  => $this->integer(11),
            'updated_by'  => $this->integer(11),
        ], $this->tableOptions);

        $this->addForeignKey(
            'fk_landings_domain_project_id',
            'landings_domain',
            'project_id',
            'landings_project',
            'id'
        );

        $this->addForeignKey(
            'fk_landings_domain_created_by',
            'landings_domain',
            'created_by',
            'users_user',
            'id'
        );
        $this->addForeignKey(
            'fk_landings_domain_updated_by',
            'landings_domain',
            'updated_by',
            'users_user',
            'id'
        );


        $this->createTable('landings_landing', [
            'id'         => $this->primaryKey(11),
            'project_id' => $this->integer(11),
            'domain_id'  => $this->integer(11),

            'name'        => $this->string(255)->notNull(),
            'description' => $this->text(),

            'type' => $this->smallInteger(1)->notNull(),//mob, desctop, preland

            'counters' => $this->text(),

            'page_ext' => $this->string(255)->notNull(),

            'transit_url' => $this->string(255),
            'transit_uri' => $this->smallInteger(1),

            'status'     => $this->smallInteger(1)->notNull(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->dateTime(),
            'created_by' => $this->integer(11),
            'updated_by' => $this->integer(11),
        ], $this->tableOptions);

        $this->addForeignKey(
            'fk_landings_landing_project_id',
            'landings_landing',
            'project_id',
            'landings_project',
            'id'
        );
        $this->addForeignKey(
            'fk_landings_landing_domain_id',
            'landings_landing',
            'domain_id',
            'landings_domain',
            'id'
        );
        $this->addForeignKey(
            'fk_landings_landing_created_by',
            'landings_landing',
            'created_by',
            'users_user',
            'id'
        );
        $this->addForeignKey(
            'fk_landings_landing_updated_by',
            'landings_landing',
            'updated_by',
            'users_user',
            'id'
        );

        $this->createTable('landings_page', [
            'id'         => $this->primaryKey(11),
            'landing_id' => $this->integer(11),

            'page' => $this->string(255)->notNull(),
            'view' => $this->string(255)->notNull(),

            'title' => $this->string(255)->notNull(),
            'meta'  => 'jsonb NULL DEFAULT \'[]\'',

            'css'    => 'jsonb NULL DEFAULT \'[]\'',
            'js'     => $this->text(),
            'script' => 'jsonb NULL DEFAULT \'[]\'',

            'type' => $this->smallInteger(1)->notNull(), // simple

            'status'     => $this->smallInteger(1)->notNull(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->dateTime(),
            'created_by' => $this->integer(11),
            'updated_by' => $this->integer(11),
        ], $this->tableOptions);

        $this->addForeignKey(
            'fk_landings_page_landing_id',
            'landings_page',
            'landing_id',
            'landings_landing',
            'id'
        );
        $this->addForeignKey(
            'fk_landings_page_created_by',
            'landings_page',
            'created_by',
            'users_user',
            'id'
        );
        $this->addForeignKey(
            'fk_landings_page_updated_by',
            'landings_page',
            'updated_by',
            'users_user',
            'id'
        );

        $this->addTypes();

        $this->afterMigrate();
    }

    public function down()
    {
        $this->dropTable('landings_project');
        $this->dropTable('landings_domain');
        $this->dropTable('landings_landing');

        $this->afterMigrate();
    }


    private function addTypes()
    {
        Variety::add('land_project_type', 'land_project_type_simple', 'Простой', 1, 1);
        Variety::add('land_domain_type', 'land_domain_type_simple', 'Простой', 1, 1);
        Variety::add('landing_type', 'landing_type_mobile', 'Мобильный', 1, 1);
        Variety::add('landing_type', 'landing_type_desktop', 'Десктоп', 2, 2);
        Variety::add('landing_type', 'landing_type_preland', 'Прокладка', 3, 3);

        Variety::add('land_page_type', 'land_page_type_index', 'Главная', 1, 1);
        Variety::add('land_page_type', 'land_page_type_lid', 'Финальная', 2, 2);
        Variety::add('land_page_type', 'land_page_type_other', 'Другое', 3, 3);
    }


    /**
     * Initializes the migration.
     * This method will set [[db]] to be the 'db' application component, if it is null.
     */
    public function init()
    {
        parent::init();
        $this->setTableOptions();
        $this->beforeMigrate();
    }

    /**
     * Устанавливаем дефолтные параметры для таблиц
     */
    private function setTableOptions()
    {
        if ($this->db->driverName === 'mysql') {
            $this->tableOptions = 'ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci';
        }
    }

    /**
     * Устанавливаем начальные параметры времени и памяти
     */
    private function beforeMigrate()
    {
        echo 'Start..' . PHP_EOL;
        $this->memory = memory_get_usage();
        $this->time   = microtime(true);
    }

    /**
     * Выводим параметры времени и памяти
     */
    private function afterMigrate()
    {
        echo 'End..' . PHP_EOL;
        echo 'Использовано памяти: ' . FileHelper::formatSizeUnits((memory_get_usage() - $this->memory)) . PHP_EOL;
        echo 'Время выполнения скрипта: ' . (microtime(true) - $this->time) . ' сек.' . PHP_EOL;
    }
}

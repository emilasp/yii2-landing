<?php
namespace emilasp\landing\backend;

use emilasp\core\CoreModule;

/**
 * Class LandingModule
 * @package emilasp\landing\backend
 */
class LandingModule extends CoreModule
{
    public function init()
    {
        parent::init();
    }
}

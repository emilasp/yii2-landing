<?php
namespace emilasp\landing\backend\behaviors;

use DOMDocument;
use emilasp\core\helpers\FileHelper;
use emilasp\landing\common\models\LandPage;
use Symfony\Component\DomCrawler\Crawler;
use yii\base\Behavior;
use yii\db\ActiveRecord;

/**
 * Class SiteCreateBehavior
 * @package emilasp\landing\behaviors
 */
class SiteCreateBehavior extends Behavior
{
    /** @var int depth to upload */
    public $depth = 3;
    /** @var string Директория для загрузки сайта */
    public $uploadFilesPath;

    public function events()
    {
        return [
            ActiveRecord::EVENT_AFTER_UPDATE => 'uploadSite',
            ActiveRecord::EVENT_AFTER_INSERT => 'uploadSite',
        ];
    }


    /**
     * Загружаем сайт
     */
    public function uploadSite()
    {
        if ($this->uploadFilesPath = $this->owner->uploadPath) {
            //FileHelper::removeDirectory($this->uploadFilesPath);

            $urls = preg_split('/\\r\\n?|\\n/', $this->owner->uploadUrl);

            foreach ($urls as $url) {
                shell_exec("wget -P $this->uploadFilesPath -r -k -l $this->depth -p -E -nc -nH {$url}");
            }

            $this->htmlToView();
        }
    }


    /** Преобразуем все html файлы во вьюхи
     * @return array
     */
    private function htmlToView()
    {
        $filesToView = $this->getFiles();

        $this->createPages($filesToView);

        return $filesToView;
    }

    /** Получаем все файлы
     * @return array
     */
    private function getFiles()
    {
        $extsViews = ['html', 'htm', 'php'];

        $directory = new \RecursiveDirectoryIterator($this->uploadFilesPath);
        $iterator  = new \RecursiveIteratorIterator($directory);
        $files     = [];
        foreach ($iterator as $info) {
            $ext  = $info->getExtension();
            $name = $info->getBasename('.' . $ext);
            $type = $info->getType();
            $path = $info->getPath() . DIRECTORY_SEPARATOR;
            if ($type !== 'dir') {
                if (in_array($ext, $extsViews)) {
                    $newName = $name . '.php';
                    if ($ext !== 'php') {
                        rename($path . $name . '.' . $ext, $path . $newName);
                    }
                    $files[$path . $name] = [
                        'page' => $name . '.' . $ext,
                        'view' => $path . $newName,
                    ];
                }
            }
        }
        return $files;
    }

    /** Создаем страницы лендинга
     *
     * @param $files
     */
    private function createPages($pages)
    {
        foreach ($pages as $page) {
            $view     = $page['view'];
            $pageName = $page['page'];

            $assets  = ['meta' => [], 'js' => [], 'jsFiles' => [], 'css' => [], 'cssFiles' => [],];
            $content = file_get_contents($view);

            if (strpos($content, '-PDFG=') !== false) {
                continue;
            }

            $crawler = new Crawler($content);

            $crawler->filter('meta')->each(function ($metas) use (&$assets) {
                foreach ($metas as $meta) {
                    $assets['meta'][] = $meta->ownerDocument->saveHTML($meta);
                }
            });

            $crawler->filter('script')->each(function ($scripts) use (&$assets) {
                foreach ($scripts as $node) {
                    if ($node->getAttribute('src')) {
                        $assets['jsFiles'][] = $node->getAttribute('src');
                    } else {
                        $js             = $node->ownerDocument->saveXML($node);
                        $assets['js'][] = str_replace(['<![CDATA[', ']]>'], '', $js) . PHP_EOL;
                    }
                    $node->parentNode->removeChild($node);
                }
            });
            /*
                    $crawler->filter('script')->reduce(function (Crawler $node, $i) {
                        // filter every other node
                        return false;
                    });*/

            $crawler->filter('link')->each(function ($links) use (&$assets) {
                foreach ($links as $node) {
                    if ($node->getAttribute('rel') && $node->getAttribute('rel') === 'stylesheet') {
                        $assets['cssFiles'][] = $node->getAttribute('href');
                    }
                    $node->parentNode->removeChild($node);
                }

            });

            $body  = $crawler->filter('body')->first()->html();

            $title = '-';
            $titleNode = $crawler->filter('title')->first();
            if ($titleNode->count()) {
                $title = $titleNode->html();
            }

            $body = $this->setPhpScripts($body);

            file_put_contents($view, $body);

            $this->createPage(
                $pageName,
                $view,
                $title,
                $assets['meta'],
                $assets['js'],
                $assets['cssFiles'],
                $assets['jsFiles']
            );
        }
    }

    private function setPhpScripts($html)
    {
        $html = PHP_EOL . '<?php $basePath = Yii::$app->assetManager->getPublishedUrl($landing->uploadPath); //-PDFG= ?>'
                . PHP_EOL . $html;
        preg_match_all('/< *img[^>]*src *= *["\']?([^"\']*)/i', $html, $matches);

        foreach ($matches[1] as $path) {
            if ($path !== 'index.html' && strpos($path, '://') === false) {
                $pathNew = ltrim($path, '/');
                $html    = str_replace(
                    ['src="' . $path, 'src=\'' . $path],
                    [
                        'src="<?= $basePath ?>/' . $pathNew,
                        'src=\'<?= $basePath ?>/' . $pathNew,
                    ],
                    $html
                );
            }
        }

        return $html;
    }

    /** Создаем страницу
     *
     * @param $file
     * @param $title
     * @param $meta
     * @param $scripts
     * @param $csses
     * @param $scriptFiles
     */
    private function createPage($page, $view, $title, $meta, $scripts, $csses, $scriptFiles)
    {
        $pageModel = LandPage::findOne(['landing_id' => $this->owner->id, 'view' => $view]);
        if (!$pageModel) {
            $pageModel = new LandPage();
        }
        $pageModel->landing_id = $this->owner->id;
        $pageModel->view       = $view;
        $pageModel->page       = $page;
        $pageModel->title      = $title ? $title : ' - ';
        $pageModel->js         = implode(PHP_EOL, $scripts);
        $pageModel->meta       = $this->getJsonData($meta, 'meta');
        $pageModel->script     = $this->getJsonData($scriptFiles, 'file');
        $pageModel->css        = $this->getJsonData($csses, 'file');
        $pageModel->type       = 1;
        $pageModel->status     = 1;
        $pageModel->save();
    }

    /** формируем  правильный JSON
     *
     * @param $array
     * @param $field
     *
     * @return array
     */
    private function getJsonData($array, $field)
    {
        $data = [];
        foreach ($array as $element) {
            $data[] = [$field => $element];
        }
        return json_encode($data);
    }
}

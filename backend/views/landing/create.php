<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model emilasp\landing\common\models\Landing */

$this->title = Yii::t('landing', 'Create Landing');
$this->params['breadcrumbs'][] = ['label' => Yii::t('landing', 'Landings'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="landing-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

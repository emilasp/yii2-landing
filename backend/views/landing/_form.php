<?php

use emilasp\landing\common\models\LandDomain;
use emilasp\landing\common\models\LandProject;
use kartik\widgets\Select2;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model emilasp\landing\common\models\Landing */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="landing-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php echo $form->errorSummary($model); ?>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'project_id')->widget(Select2::classname(), [
                'language'      => 'ru',
                'data'          => LandProject::find()->map()->all(),
                'options'       => ['placeholder' => 'Выберите проект..'],
                'pluginOptions' => [
                    'allowClear' => true,
                ],
            ]) ?>

            <?= $form->field($model, 'domain_id')->widget(Select2::classname(), [
                'language'      => 'ru',
                'data'          => LandDomain::find()->map()->all(),
                'options'       => ['placeholder' => 'Выберите домен..'],
                'pluginOptions' => [
                    'allowClear' => true,
                ],
            ]) ?>

            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'description')->textarea(['rows' => 3]) ?>

            <?= $form->field($model, 'page_ext')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'type')->dropDownList($model->types) ?>

            <?= $form->field($model, 'status')->dropDownList($model->statuses) ?>
        </div>
        <div class="col-md-6">

            <div class="row">
                <div class="col-md-9">
                    <?= $form->field($model, 'transit_url')->textInput(['maxlength' => true]) ?>
                </div>
                <div class="col-md-3">
                    <?= $form->field($model, 'transit_uri')->checkbox() ?>
                </div>
            </div>

            <?= $form->field($model, 'counters')->textarea(['rows' => 10]) ?>

            <hr />

            <?= $form->field($model, 'uploadUrl')->textarea(['rows' => 4]) ?>

        </div>
    </div>



    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('site', 'Create') : Yii::t('site', 'Update'),
            ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

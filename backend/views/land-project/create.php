<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model emilasp\landing\common\models\LandProject */

$this->title = Yii::t('landing', 'Create Land Project');
$this->params['breadcrumbs'][] = ['label' => Yii::t('landing', 'Land Projects'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="land-project-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

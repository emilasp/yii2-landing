<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model emilasp\landing\common\models\LandProject */

$this->title = Yii::t('site', 'Update {modelClass}: ', [
    'modelClass' => Yii::t('landing', 'Land Project'),
]) . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('landing', 'Land Projects'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('site', 'Update');
?>
<div class="land-project-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

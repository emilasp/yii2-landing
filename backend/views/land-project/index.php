<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel emilasp\landing\common\models\search\LandProjectSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('landing', 'Land Projects');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="land-project-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('landing', 'Create Land Project'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
<?php Pjax::begin(); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => '\kartik\grid\SerialColumn'],
            [
                'attribute' => 'id',
                'class'     => '\kartik\grid\DataColumn',
                'width'     => '100px',
                'hAlign'    => GridView::ALIGN_CENTER,
                'vAlign'    => GridView::ALIGN_MIDDLE,
            ],
            'name',
            'description:ntext',
            [
                'attribute' => 'type',
                'value'     => function ($model, $key, $index, $column) {
                    return $model->types[$model->type];
                },
                'class'     => '\kartik\grid\DataColumn',
                'hAlign'    => GridView::ALIGN_LEFT,
                'vAlign'    => GridView::ALIGN_MIDDLE,
                'width'     => '150px',
                'filter'    => $searchModel->types,
            ],
            [
                'attribute' => 'status',
                'value'     => function ($model, $key, $index, $column) {
                    return $model->statuses[$model->status];
                },
                'class'     => '\kartik\grid\DataColumn',
                'hAlign'    => GridView::ALIGN_LEFT,
                'vAlign'    => GridView::ALIGN_MIDDLE,
                'width'     => '150px',
                'filter'    => $searchModel->statuses,
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
<?php Pjax::end(); ?></div>


<div class="item panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title pull-left"><?= Yii::t('site', 'Meta') ?></h3>

        <div class="pull-right">
            <button type="button" class="add-item btn btn-success btn-xs"><i
                    class="glyphicon glyphicon-plus"></i></button>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="panel-body">

        <?php foreach ($models as $i => $model) : ?>
            <div class="row row-item">
                <div class="col-sm-10">
                    <?= $form->field($model, "[{$i}]meta")
                        ->textInput()
                        ->label(false) ?>
                </div>
                <div class="col-sm-2">
                    <button type="button" class="remove-item btn btn-danger btn-xs">
                        <i class="glyphicon glyphicon-minus"></i>
                    </button>
                </div>
            </div>
        <?php endforeach ?>
    </div>
</div>

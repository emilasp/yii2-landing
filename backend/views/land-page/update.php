<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model emilasp\landing\common\models\LandPage */

$this->title = Yii::t('site', 'Update {modelClass}: ', [
    'modelClass' => Yii::t('landing', 'Land Page'),
]) . $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('landing', 'Land Pages'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('site', 'Update');
?>
<div class="land-page-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

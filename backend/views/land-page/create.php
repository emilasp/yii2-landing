<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model emilasp\landing\common\models\LandPage */

$this->title = Yii::t('landing', 'Create Land Page');
$this->params['breadcrumbs'][] = ['label' => Yii::t('landing', 'Land Pages'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="land-page-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

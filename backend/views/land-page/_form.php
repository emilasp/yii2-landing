<?php

use emilasp\json\models\DynamicModel;
use emilasp\json\widgets\DynamicFields\DynamicFields;
use emilasp\landing\common\models\Landing;
use kartik\widgets\Select2;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model emilasp\landing\common\models\LandPage */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="land-page-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php echo $form->errorSummary($model); ?>

    <div class="row">
        <div class="col-md-6">

            <?= $form->field($model, 'landing_id')->widget(Select2::classname(), [
                'language'      => 'ru',
                'data'          => Landing::find()->map()->all(),
                'options'       => ['placeholder' => 'Выберите лендинг..'],
                'pluginOptions' => [
                    'allowClear' => true,
                ],
            ]) ?>

            <?= $form->field($model, 'page')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'view')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'type')->dropDownList($model->types) ?>

            <?= $form->field($model, 'status')->dropDownList($model->statuses) ?>
        </div>
        <div class="col-md-6">

            <?= DynamicFields::widget([
                'form'        => $form,
                'model'       => $model,
                'attribute'   => 'meta',
                'scheme'      => DynamicModel::SCHEME_CUSTOM,
                'addEmptyRow' => false,
                'viewFile'  => '@vendor/emilasp/yii2-landing/backend/views/land-page/json/_meta',
            ]); ?>

            <?= DynamicFields::widget([
                'form'        => $form,
                'model'       => $model,
                'attribute'   => 'css',
                'scheme'      => DynamicModel::SCHEME_CUSTOM,
                'addEmptyRow' => false,
                'viewFile'  => '@vendor/emilasp/yii2-landing/backend/views/land-page/json/_css',
            ]); ?>

            <?= DynamicFields::widget([
                'form'        => $form,
                'model'       => $model,
                'attribute'   => 'script',
                'scheme'      => DynamicModel::SCHEME_CUSTOM,
                'addEmptyRow' => false,
                'viewFile'  => '@vendor/emilasp/yii2-landing/backend/views/land-page/json/_js',
            ]); ?>

            <?= $form->field($model, 'js')->textarea(['rows' => 8]) ?>

        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('site', 'Create') : Yii::t('site', 'Update'),
            ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model emilasp\landing\common\models\LandDomain */

$this->title = Yii::t('landing', 'Create Land Domain');
$this->params['breadcrumbs'][] = ['label' => Yii::t('landing', 'Land Domains'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="land-domain-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

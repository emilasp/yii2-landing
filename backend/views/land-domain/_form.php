<?php

use emilasp\landing\common\models\LandProject;
use kartik\widgets\Select2;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model emilasp\landing\common\models\LandDomain */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="land-domain-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php echo $form->errorSummary($model); ?>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'description')->textarea(['rows' => 3]) ?>

            <?= $form->field($model, 'type')->dropDownList($model->types) ?>

            <?= $form->field($model, 'status')->dropDownList($model->statuses) ?>
        </div>
        <div class="col-md-6">

            <?= $form->field($model, 'project_id')->widget(Select2::classname(), [
                'language'      => 'ru',
                'data'          => LandProject::find()->map()->all(),
                'options'       => ['placeholder' => 'Выберите проект..'],
                'pluginOptions' => [
                    'allowClear' => true,
                ],
            ]) ?>

            <div class="row">
                <div class="col-md-6">
                    <?= $form->field($model, 'sub')->textInput(['maxlength' => true]) ?>
                </div>
                <div class="col-md-6">
                    <?= $form->field($model, 'domain')->textInput(['maxlength' => true]) ?>
                </div>
            </div>
            
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('site', 'Create') : Yii::t('site', 'Update'),
            ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

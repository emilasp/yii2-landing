<?php

namespace emilasp\landing\common\models;

use emilasp\core\components\base\ActiveRecord;
use emilasp\user\core\models\User;
use emilasp\variety\behaviors\VarietyModelBehavior;
use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "landings_domain".
 *
 * @property integer $id
 * @property integer $project_id
 * @property string $name
 * @property string $domain
 * @property string $sub
 * @property string $description
 * @property integer $type
 * @property integer $status
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 *
 * @property LandProject $project
 * @property User $createdBy
 * @property User $updatedBy
 * @property Landing[] $landings
 */
class LandDomain extends ActiveRecord
{
    /**
     * @return array
     */
    public function behaviors()
    {
        return ArrayHelper::merge([
            'variety_status' => [
                'class'     => VarietyModelBehavior::className(),
                'attribute' => 'status',
                'group'     => 'status',
            ],
            'variety_type'   => [
                'class'     => VarietyModelBehavior::className(),
                'attribute' => 'type',
                'group'     => 'land_domain_type',
            ],
            [
                'class' => TimestampBehavior::className(),
                'value' => new Expression('NOW()'),
            ],
            [
                'class' => BlameableBehavior::className(),
                'value' => function () {
                    if (!isset(Yii::$app->user)) {
                        return 1;
                    }
                    return Yii::$app->user->id;
                },
            ],
        ], parent::behaviors());
    }


    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'landings_domain';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['project_id', 'type', 'status', 'created_by', 'updated_by'], 'integer'],
            [['name', 'domain', 'sub', 'type', 'status'], 'required'],
            [['description'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['name'], 'string', 'max' => 255],
            [['domain'], 'string', 'max' => 30],
            [['sub'], 'string', 'max' => 15],
            [
                ['project_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => LandProject::className(),
                'targetAttribute' => ['project_id' => 'id'],
            ],
            [
                ['created_by'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => User::className(),
                'targetAttribute' => ['created_by' => 'id'],
            ],
            [
                ['updated_by'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => User::className(),
                'targetAttribute' => ['updated_by' => 'id'],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'          => Yii::t('site', 'ID'),
            'project_id'  => Yii::t('landing', 'Land Project'),
            'name'        => Yii::t('site', 'Name'),
            'domain'      => Yii::t('landing', 'Land Domain'),
            'sub'         => Yii::t('landing', 'Sub'),
            'description' => Yii::t('site', 'Description'),
            'type'        => Yii::t('site', 'Type'),
            'status'      => Yii::t('site', 'Status'),
            'created_at'  => Yii::t('site', 'Created At'),
            'updated_at'  => Yii::t('site', 'Updated At'),
            'created_by'  => Yii::t('site', 'Created By'),
            'updated_by'  => Yii::t('site', 'Updated By'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProject()
    {
        return $this->hasOne(LandProject::className(), ['id' => 'project_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLandingsLandings()
    {
        return $this->hasMany(Landing::className(), ['domain_id' => 'id']);
    }
}

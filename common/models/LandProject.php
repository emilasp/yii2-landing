<?php

namespace emilasp\landing\common\models;

use emilasp\core\components\base\ActiveRecord;
use emilasp\user\core\models\User;
use emilasp\variety\behaviors\VarietyModelBehavior;
use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "landings_project".
 *
 * @property integer      $id
 * @property string       $name
 * @property string       $description
 * @property integer      $type
 * @property integer      $status
 * @property string       $created_at
 * @property string       $updated_at
 * @property integer      $created_by
 * @property integer      $updated_by
 *
 * @property LandDomain[] $domains
 * @property Landing[]    $landings
 * @property User         $createdBy
 * @property User         $updatedBy
 */
class LandProject extends ActiveRecord
{
    /**
     * @return array
     */
    public function behaviors()
    {
        return ArrayHelper::merge([
            'variety_status' => [
                'class'     => VarietyModelBehavior::className(),
                'attribute' => 'status',
                'group'     => 'status',
            ],
            'variety_type' => [
                'class'     => VarietyModelBehavior::className(),
                'attribute' => 'type',
                'group'     => 'land_project_type',
            ],
            [
                'class' => TimestampBehavior::className(),
                'value' => new Expression('NOW()'),
            ],
            [
                'class' => BlameableBehavior::className(),
                'value' => function () {
                    if (!isset(Yii::$app->user)) {
                        return 1;
                    }
                    return Yii::$app->user->id;
                },
            ],
        ], parent::behaviors());
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'landings_project';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'type', 'status'], 'required'],
            [['description'], 'string'],
            [['type', 'status', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['name'], 'string', 'max' => 255],
            [
                ['created_by'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => User::className(),
                'targetAttribute' => ['created_by' => 'id']
            ],
            [
                ['updated_by'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => User::className(),
                'targetAttribute' => ['updated_by' => 'id']
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'          => Yii::t('site', 'ID'),
            'name'        => Yii::t('site', 'Name'),
            'description' => Yii::t('site', 'Description'),
            'type'        => Yii::t('site', 'Type'),
            'status'      => Yii::t('site', 'Status'),
            'created_at'  => Yii::t('site', 'Created At'),
            'updated_at'  => Yii::t('site', 'Updated At'),
            'created_by'  => Yii::t('site', 'Created By'),
            'updated_by'  => Yii::t('site', 'Updated By'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLandingsDomains()
    {
        return $this->hasMany(LandDomain::className(), ['project_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLandingsLandings()
    {
        return $this->hasMany(Landing::className(), ['project_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }
}

<?php

namespace emilasp\landing\common\models;

use emilasp\core\components\base\ActiveRecord;
use emilasp\json\behaviors\JsonFieldBehavior;
use emilasp\user\core\models\User;
use emilasp\variety\behaviors\VarietyModelBehavior;
use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "landings_page".
 *
 * @property integer $id
 * @property integer $landing_id
 * @property string $page
 * @property string $view
 * @property string $title
 * @property string $meta
 * @property string $css
 * @property string $js
 * @property string $script
 * @property integer $type
 * @property integer $status
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 *
 * @property Landing $landing
 * @property User $createdBy
 * @property User $updatedBy
 */
class LandPage extends ActiveRecord
{
    /**
     * @return array
     */
    public function behaviors()
    {
        return ArrayHelper::merge([
            'variety_status' => [
                'class'     => VarietyModelBehavior::className(),
                'attribute' => 'status',
                'group'     => 'status',
            ],
            'variety_type'   => [
                'class'     => VarietyModelBehavior::className(),
                'attribute' => 'type',
                'group'     => 'land_page_type',
            ],
            [
                'class'     => JsonFieldBehavior::className(),
                'attribute' => 'css',
                'scheme'    => [
                    'file' => ['label' => 'CSS', 'rules' => [['string']]],
                ],
            ],
            [
                'class'     => JsonFieldBehavior::className(),
                'attribute' => 'script',
                'scheme'    => [
                    'file' => ['label' => 'JS', 'rules' => [['string']]],
                ],
            ],
            [
                'class'     => JsonFieldBehavior::className(),
                'attribute' => 'meta',
                'scheme'    => [
                    'meta' => ['label' => 'Meta', 'rules' => [['string']]],
                ],
            ],
            [
                'class' => TimestampBehavior::className(),
                'value' => new Expression('NOW()'),
            ],
            [
                'class' => BlameableBehavior::className(),
                'value' => function () {
                    if (!isset(Yii::$app->user)) {
                        return 1;
                    }
                    return Yii::$app->user->id;
                },
            ],
        ], parent::behaviors());
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'landings_page';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['landing_id', 'type', 'status', 'created_by', 'updated_by'], 'integer'],
            [['title', 'page', 'view', 'status'], 'required'],
            [['meta', 'css', 'js', 'script', 'page', 'view'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['title'], 'string', 'max' => 255],
            [
                ['landing_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => Landing::className(),
                'targetAttribute' => ['landing_id' => 'id'],
            ],
            [
                ['created_by'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => User::className(),
                'targetAttribute' => ['created_by' => 'id'],
            ],
            [
                ['updated_by'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => User::className(),
                'targetAttribute' => ['updated_by' => 'id'],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'         => Yii::t('site', 'ID'),
            'landing_id' => Yii::t('landing', 'Landing'),
            'page'       => Yii::t('landing', 'Page'),
            'view'       => Yii::t('landing', 'View'),
            'title'      => Yii::t('landing', 'Title'),
            'meta'       => Yii::t('landing', 'Meta'),
            'css'        => Yii::t('landing', 'Css'),
            'js'         => Yii::t('landing', 'Js'),
            'script'     => Yii::t('landing', 'Js файлы'),
            'type'       => Yii::t('site', 'Type'),
            'status'     => Yii::t('site', 'Status'),
            'created_at' => Yii::t('site', 'Created At'),
            'updated_at' => Yii::t('site', 'Updated At'),
            'created_by' => Yii::t('site', 'Created By'),
            'updated_by' => Yii::t('site', 'Updated By'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLanding()
    {
        return $this->hasOne(Landing::className(), ['id' => 'landing_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }
}

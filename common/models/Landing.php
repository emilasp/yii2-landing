<?php

namespace emilasp\landing\common\models;

use emilasp\core\components\base\ActiveRecord;
use emilasp\landing\backend\behaviors\SiteCreateBehavior;
use emilasp\user\core\models\User;
use emilasp\variety\behaviors\VarietyModelBehavior;
use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "landings_landing".
 *
 * @property integer     $id
 * @property integer     $project_id
 * @property integer     $domain_id
 * @property string      $name
 * @property string      $description
 * @property integer     $type
 * @property string      $page_ext
 * @property string      $counters
 * @property string      $transit_url
 * @property integer     $transit_uri
 * @property integer     $status
 * @property string      $created_at
 * @property string      $updated_at
 * @property integer     $created_by
 * @property integer     $updated_by
 *
 * @property string     $uploadUrl
 *
 * @property LandDomain  $domain
 * @property LandProject $project
 * @property User        $createdBy
 * @property User        $updatedBy
 */
class Landing extends ActiveRecord
{
    public $uploadUrl;

    /**
     * @return array
     */
    public function behaviors()
    {
        return ArrayHelper::merge([
            'variety_status' => [
                'class'     => VarietyModelBehavior::className(),
                'attribute' => 'status',
                'group'     => 'status',
            ],
            'variety_type' => [
                'class'     => VarietyModelBehavior::className(),
                'attribute' => 'type',
                'group'     => 'landing_type',
            ],
            'uploadSite' => [
                'class'     => SiteCreateBehavior::className(),
            ],
            [
                'class' => TimestampBehavior::className(),
                'value' => new Expression('NOW()'),
            ],
            [
                'class' => BlameableBehavior::className(),
                'value' => function () {
                    if (!isset(Yii::$app->user)) {
                        return 1;
                    }
                    return Yii::$app->user->id;
                },
            ],
        ], parent::behaviors());
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'landings_landing';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'project_id',
                    'domain_id',
                    'type',
                    'transit_uri',
                    'status',
                    'created_by',
                    'updated_by'
                ],
                'integer'
            ],
            [['name', 'type', 'status'], 'required'],
            [['description', 'counters', 'uploadUrl'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['name', 'page_ext', 'transit_url'], 'string', 'max' => 255],
            [
                ['domain_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => LandDomain::className(),
                'targetAttribute' => ['domain_id' => 'id']
            ],
            [
                ['project_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => LandProject::className(),
                'targetAttribute' => ['project_id' => 'id']
            ],
            [
                ['created_by'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => User::className(),
                'targetAttribute' => ['created_by' => 'id']
            ],
            [
                ['updated_by'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => User::className(),
                'targetAttribute' => ['updated_by' => 'id']
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'          => Yii::t('landing', 'ID'),
            'project_id'  => Yii::t('landing', 'Land Project'),
            'domain_id'   => Yii::t('landing', 'Land Domain'),
            'name'        => Yii::t('site', 'Name'),
            'description' => Yii::t('site', 'Description'),
            'type'        => Yii::t('site', 'Type'),
            'page_ext'    => Yii::t('landing', 'Page Extension'),
            'counters'    => Yii::t('landing', 'Counters'),
            'transit_url' => Yii::t('landing', 'Transit Url'),
            'transit_uri' => Yii::t('landing', 'Transit Uri'),
            'status'      => Yii::t('site', 'Status'),
            'created_at'  => Yii::t('site', 'Created At'),
            'updated_at'  => Yii::t('site', 'Updated At'),
            'created_by'  => Yii::t('site', 'Created By'),
            'updated_by'  => Yii::t('site', 'Updated By'),

            'uploadUrl'  => Yii::t('site', 'Адрес сайта для загрузки'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDomain()
    {
        return $this->hasOne(LandDomain::className(), ['id' => 'domain_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProject()
    {
        return $this->hasOne(LandProject::className(), ['id' => 'project_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }
}

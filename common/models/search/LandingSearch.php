<?php

namespace emilasp\landing\common\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use emilasp\landing\common\models\Landing;

/**
 * LandingSearch represents the model behind the search form about `emilasp\landing\common\models\Landing`.
 */
class LandingSearch extends Landing
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'project_id', 'domain_id', 'type', 'transit_uri', 'status', 'created_by', 'updated_by'], 'integer'],
            [['name', 'description', 'counters', 'page_ext', 'transit_url', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Landing::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'project_id' => $this->project_id,
            'domain_id' => $this->domain_id,
            'type' => $this->type,
            'transit_uri' => $this->transit_uri,
            'status' => $this->status,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'counters', $this->counters])
            ->andFilterWhere(['like', 'page_ext', $this->page_ext])
            ->andFilterWhere(['like', 'transit_url', $this->transit_url]);

        return $dataProvider;
    }
}

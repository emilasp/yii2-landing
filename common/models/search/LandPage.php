<?php

namespace emilasp\landing\common\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use emilasp\landing\common\models\LandPage as LandPageModel;

/**
 * LandPage represents the model behind the search form about `emilasp\landing\common\models\LandPage`.
 */
class LandPage extends LandPageModel
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'landing_id', 'type', 'status', 'created_by', 'updated_by'], 'integer'],
            [['page', 'view', 'title', 'meta', 'css', 'js',  'script', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = LandPageModel::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id'         => $this->id,
            'landing_id' => $this->landing_id,
            'status'     => $this->status,
            'type'       => $this->type,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['like', 'title', $this->title])
              ->andFilterWhere(['like', 'view', $this->view])
              ->andFilterWhere(['like', 'page', $this->page])
              ->andFilterWhere(['like', 'js', $this->js]);
              /*->andFilterWhere(['like', 'css', $this->css])
              ->andFilterWhere(['like', 'script', $this->script])*/

        return $dataProvider;
    }
}

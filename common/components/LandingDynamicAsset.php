<?php
namespace emilasp\landing\common\components;

use yii\web\AssetBundle;

/**
 * Class LandingDynamicAsset
 * @package emilasp\landing\common\components
 */
class LandingDynamicAsset extends AssetBundle
{
    public static $jsFiles;
    public static $cssFiles;
    public static $source;

    public $sourcePath = __DIR__ . '/assets';

    public $js  = [];
    public $css = [];

    public $depends = [];


    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->sourcePath = self::$source;
        $this->setAssets();
        parent::init();
    }

    /**
     * Аттачим ассеты для ленда
     */
    private function setAssets()
    {
        /* JS */
        foreach (self::$jsFiles as $index => $file) {
            $this->js[] = $file;
        }
        /* CSS */
        foreach (self::$cssFiles as $index => $file) {
            $this->css[] = $file;
        }
    }
}
